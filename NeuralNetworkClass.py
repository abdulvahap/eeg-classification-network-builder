# import pandas library to work with data
import numpy
import pandas as pd
# import train_test_split to split data
from sklearn.model_selection import train_test_split
# import minimum maximum scaler to transform data
from sklearn.preprocessing import StandardScaler
# import necessary libraries to work with neural networks
from tensorflow.python.framework.random_seed import set_random_seed
from tensorflow.python.keras import Sequential
from tensorflow.python.keras.layers import Dense
# import confusion matrix and accuracy score library to visualise and calculate accuracy
from sklearn.metrics import confusion_matrix, accuracy_score
# import pyplot to display confusion matrix in an image
import matplotlib.pyplot as plt
# import timeit library to calculate the prediction time
import timeit
# import seed library to have the same results
from numpy.random import seed


class NeuralNetwork:
    """
    A class to represent a multi layer perceptron with two hidden layers.
   ...

    Attributes
    ----------
    seed : str
        first name of the person
    seed2 : str
        Attribute to represent the global seed in order to set global random seed.
    neural_network : Sequential
        Attribute to represent neural network model. Sequential class groups a linear stack of layers into a model.
    history : History
        Attribute to represent a History object. Its History.history attribute is a record of training loss values
        and metrics values at successive epochs,
        as well as validation loss values and validation metrics values (if applicable).
    accuracy :
        Attribute to represent accuracy of the model

    ...


    Methods
    -------
    read_data():
        Reads the data from csv file
    log_data_info():
        Logs dataset information to see data structure
    ...

    """
    def __init__(self):
        self.seed = seed(1)
        self.seed2 = set_random_seed(1)
        self.neural_network = self.create_neural_network_model()
        self.history = self.train_neural_network(self.neural_network)
        self.accuracy = self.calculate_and_visualize_network_accuracy(self.neural_network)

    # read the data file to work with it in the code
    @staticmethod
    def read_data():
        dataset = pd.read_csv('data.csv')
        return dataset

    # log dataset information to see data structure
    @staticmethod
    def log_data_info(dataset):
        # display the dataset head
        print("Dataset head : \n")
        print(dataset.head())
        # display the dataset tail
        print("Dataset tail : \n")
        print(dataset.tail())
        # print the shape of the dataset
        print("Dataset shape : \n")
        print(dataset.shape)

    # extract input and output data to use in the neural network
    def extract_input_and_output(self):
        dataset = self.read_data()
        # extract input data
        input_data = dataset.iloc[:, 1:179].values
        # extract output data
        output_data = dataset.iloc[:, 179:].values
        return input_data, output_data

    # reduce the number of output classes to simplify classification
    @staticmethod
    def reduce_number_of_output_classes(output_data):
        for i in range(len(output_data)):
            if output_data[i] == 1:
                output_data[i] = 1
            else:
                output_data[i] = 0
        return output_data

    # split data into training and test data
    def split_data_as_training_and_test(self):
        input_data, output_data = self.extract_input_and_output()
        output_data = self.reduce_number_of_output_classes(output_data)
        input_training_data, input_test_data, output_training_data, actual_output_test_data \
            = train_test_split(input_data, output_data, train_size=0.8)
        scaled_input_training_data, scaled_input_test_data = self.scale_data(input_training_data, input_test_data)
        return scaled_input_training_data, scaled_input_test_data, output_training_data, actual_output_test_data

    # log training and test data shape to see how data is split
    @staticmethod
    def log_training_and_test_data_shapes(input_training_data, input_test_data,
                                          output_training_data, actual_output_test_data):
        # print the shape of input_training_data
        print("Input training data : \n")
        print( input_training_data.shape)
        # print the shape of input_test_data
        print("Input test data : \n")
        print(input_test_data.shape)
        # print the shape of output_training_data
        print("Output training data shape : \n")
        print(output_training_data.shape)
        # print the shape of actual_output_test_data
        print("Actual output test data : \n")
        print(actual_output_test_data.shape)

    # scale data to get electrode voltage signals in an uniform range
    @staticmethod
    def scale_data(input_training_data, input_test_data):
        scaler = StandardScaler()
        scaled_input_training_data = scaler.fit_transform(input_training_data)
        scaled_input_test_data = scaler.fit_transform(input_test_data)
        return scaled_input_training_data, scaled_input_test_data

    # create a neural network to train
    @staticmethod
    def create_neural_network_model():
        # initialize a neural network model by using Sequential() class
        neural_network_model = Sequential()
        # add the first layer to the model
        neural_network_model.add(Dense(units=32, kernel_initializer='uniform', activation='relu', input_dim=178, use_bias=True))
        # add the second layer to the model
        neural_network_model.add(Dense(units=32, kernel_initializer='uniform', activation='relu', use_bias=True))
        # add the output layer to the model
        neural_network_model.add(Dense(units=1, kernel_initializer='uniform', activation='sigmoid', use_bias=True))
        return neural_network_model

    # train neural to perform classification for EEG signals
    def train_neural_network(self, neural_network_model):
        input_training_data, input_test_data, \
        output_training_data, actual_output_test_data = self.split_data_as_training_and_test()
        # configure the model for training
        neural_network_model.compile(optimizer='Adam', loss='binary_crossentropy', metrics=['accuracy'])
        # train the model for a fixed number of epochs (iterations on a dataset)
        history = neural_network_model.fit(input_training_data, output_training_data, batch_size=25, epochs=25,
                                           validation_data=(input_test_data, actual_output_test_data))
        return history

    # log neural network information
    @staticmethod
    def log_neural_network_info(neural_network_model):
        # print the summary of the network
        print("Neural network model summary : \n")
        print(neural_network_model.summary())

    # make predictions with neural network to see accuracy of the network
    def make_predictions(self, neural_network_model):
        _, input_test_data, _, actual_output_test_data = self.split_data_as_training_and_test()
        # predict and store predictions for testing data
        output_predictions = neural_network_model.predict(input_test_data)
        # Convert output predictions into boolean format
        output_predictions = (output_predictions > 0.5)
        return actual_output_test_data, output_predictions

    def calculate_and_visualize_network_accuracy(self, neural_network_model):
        actual_output_test_data, output_predictions = self.make_predictions(neural_network_model)
        model_confusion_matrix = confusion_matrix(actual_output_test_data, output_predictions)
        print("Neural network confusion matrix :")
        print(model_confusion_matrix)
        # plot rectangular data as a color-encoded matrix.
        accuracy = accuracy_score(actual_output_test_data, output_predictions)
        print("Accuracy score :" + str(accuracy))
        return accuracy

    def calculate_prediction_duration(self, neural_network_model):
        _, input_test_data, _, _ = self.split_data_as_training_and_test()
        start_time = timeit.default_timer()
        neural_network_model.predict(input_test_data)
        stop_time = timeit.default_timer()
        test_duration = stop_time - start_time
        print("Test duration in seconds: " + str(test_duration))

    # https://machinelearningmastery.com/display-deep-learning-model-training-history-in-keras/
    # summarize history for accuracy
    @staticmethod
    def plot_model_accuracy(history):
        plot1 = plt.figure(1)
        plt.plot(history.history['accuracy'])
        plt.plot(history.history['val_accuracy'])
        plt.title('Model Accuracy on Training and Validation Data')
        plt.ylabel('Accuracy')
        plt.xlabel('Epochs')
        plt.legend(['Training Data', 'Validation Data'], loc='upper left')
        plt.show()

    # https://machinelearningmastery.com/display-deep-learning-model-training-history-in-keras/
    # summarize history for loss
    @staticmethod
    def plot_model_loss(history):
        plot1 = plt.figure(2)
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('Model Loss on Training and Validation Data')
        plt.ylabel('Loss')
        plt.xlabel('Epochs')
        plt.legend(['Training Data', 'Validation Data'], loc='upper left')
        plt.show()

    # put input and actual output test data into csv files to test in C code
    def put_test_data_into_csv_file(self):
        _, input_test_data, _, actual_output_test_data = self.split_data_as_training_and_test()
        # slice each element to avoid memory errors in c code
        input_test_data = numpy.around(input_test_data, decimals=9)
        # put scaled input test data in data frame
        scaled_input_test_data = pd.DataFrame(input_test_data)
        # put scaled input test data in a csv file
        scaled_input_test_data.to_csv("in.csv", index=False, header=False)
        # put output test data in data frame
        scaled_actual_output_test_data = pd.DataFrame(actual_output_test_data)
        # put scaled output test data in a csv file
        scaled_actual_output_test_data.to_csv("out.csv", index=False, header=False)

    # put network parameters into csv files to use in C code
    @staticmethod
    def put_network_parameters_into_csv_file(neural_network_model):

        weights_and_biases = neural_network_model.get_weights()
        df1 = pd.DataFrame(weights_and_biases[0])
        df1.to_csv("one.csv", index=False, header=False)
        df2 = pd.DataFrame(weights_and_biases[2])
        df2.to_csv("two.csv", index=False, header=False)
        df3 = pd.DataFrame(weights_and_biases[4])
        df3.to_csv("three.csv", index=False, header=False)

        df1 = pd.DataFrame(weights_and_biases[1])
        df1.to_csv("bias1.csv", index=False, header=False)
        df2 = pd.DataFrame(weights_and_biases[3])
        df2.to_csv("bias2.csv", index=False, header=False)
        df3 = pd.DataFrame(weights_and_biases[5])
        df3.to_csv("bias3.csv", index=False, header=False)


if __name__ == '__main__':
    # Create and train a neural network to classify EEG signals
    my_neural_network = NeuralNetwork()

    dataset = NeuralNetwork.read_data()
    NeuralNetwork.log_data_info(dataset)
    x, y, z, q = NeuralNetwork.split_data_as_training_and_test(my_neural_network)
    NeuralNetwork.log_training_and_test_data_shapes(x, y, z, q)

    # Export test data and network params to use in C code
    NeuralNetwork.put_test_data_into_csv_file(my_neural_network)
    NeuralNetwork.put_network_parameters_into_csv_file(my_neural_network.neural_network)
    # Print the neural network model information
    NeuralNetwork.log_neural_network_info(my_neural_network.neural_network)
    # Print the accuracy of the neural network model
    print(my_neural_network.accuracy)
    # Print the duration of the predictions
    my_neural_network.calculate_prediction_duration(my_neural_network.neural_network)
    NeuralNetwork.plot_model_accuracy(my_neural_network.history)
    NeuralNetwork.plot_model_loss(my_neural_network.history)
